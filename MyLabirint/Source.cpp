#include <iostream>
#include <fstream>
using namespace std;
const int X = 12;
const int Y = 32;
class Stack;
class _Pos
{
public:
	int x, y;
	_Pos* next;
	_Pos()
	{}
	_Pos(int _x, int _y) :x(_x), y(_y)
	{}
	bool operator==(_Pos c1)
	{
		if (x == c1.x&&y == c1.y)
			return true;
		return false;
	}
	void ins_coord(int _x, int _y)
	{
		x = _x;
		y = _y;
	}
	static int pr_direct(_Pos& pBeg, _Pos& pEnd, float d)
	{
		_Pos temp(pBeg.x - pEnd.x, pBeg.y - pEnd.y);
		if (temp.x == 0 && temp.y<0)
			return 1;
		if (temp.x>0 && temp.y<0)
		if (d>-5)
		{
			return 2;
		}
		else
		{
			return 3;
		}
		if (temp.x<0 && temp.y<0)
		if (d>-5)
		{
			return 3;
		}
		else
		{
			return 2;
		}
		if (temp.x>0 && temp.y == 0)
			return 4;
		if (temp.x<0 && temp.y == 0)
			return 5;
		if (temp.x>0 && temp.y>0)
		if (d>-5)
		{
			return 6;
		}
		else
		{
			return 2;
		}
		if (temp.x<0 && temp.y>0)
		if (d >-5)
		{
			return 7;
		}
		else
		{
			return 3;
		}
		return 7;
	}
	void move(int _x, int _y)
	{
		x += _x;
		y += _y;
	}
	bool moveUp(Stack* way, bool arr[X][Y]);
	bool moveDown(Stack* way, bool arr[X][Y]);
	bool moveRight(Stack* way, bool arr[X][Y]);
	bool moveLeft(Stack* way, bool arr[X][Y]);
};
class Stack
{
private:
	_Pos* first;
public:
	static int count;
	int c_back;
	Stack() :first(NULL)
	{
		count = 0;
		c_back = 0;
	}
	_Pos* put(int _x, int _y)
	{
		_Pos* new_link = new _Pos(_x, _y);
		new_link->next = first;
		first = new_link;
		count++;
		return first->next;
	}
	bool isLoop(_Pos& loop)
	{
		_Pos* temp = &loop;
		while (temp->next)
		{
			if (*temp->next == loop)
				return true;
			temp = temp->next;
		}
		return false;
	}
	void put(_Pos & curr)
	{
		_Pos* new_link = new _Pos(curr.x, curr.y);
		new_link->next = first;
		curr.next = first;
		first = new_link;
		count++;
	}

	_Pos* get()
	{
		_Pos* current = first;
		first = current->next;
		count--;
		c_back++;
		return current;
	}
};
void show_way(Stack & st)
{
	bool arr[X][Y];
	for (int x = 0; x<X; x++)
	{
		for (int y = 0; y<Y; y++)
		{
			arr[x][y] = 1;
		}
	}
	_Pos* temp;
	do
	{
		temp = st.get();
		arr[temp->x][temp->y] = 0;

	} while ((temp->next));
	cout << endl;
	for (int x = 0; x<X; x++)
	{
		for (int y = 0; y<Y; y++)
		{
			if (arr[x][y] == 0)
			{
				cout << "0";
			}
			else
			{
				cout << "1";
			}

		}
		cout << endl;
	}
}
int Stack::count;
/////////////////////////////////////////
int  main()
{
	ifstream file;
	bool map_lab[X][Y];
	char t;
	file.open("array.txt", ios_base::in);
	if (!file)
	{
		cout << "error";
		cin.get();
	}
	for (int x = 0; x<X; x++)
	{
		for (int y = 0; y<Y; y++)
		{
			file >> t;
			cout << t;
			map_lab[x][y] = atoi(&t);
		}
		cout << endl;
	}
	////////////////////////////////////
	Stack lab;
	_Pos current, pExit;
	float per_err = 0, delta;
	for (int x = 0; x < X; x++)   //����� ����� �� ������ �������
	{
		if (map_lab[x][0] == 0)  //���� ����� ����, �������� ���������� ������� ������� � ������ �� � ����+ ���������� ������ �� 1 �������
		{
			current.ins_coord(x, 0);
			lab.put(current);
			current.move(0, 1);
			lab.put(current);
			break;
		}
	}
	for (int y = 0; y < Y; y++)   //����� ����� ������ �� ���� ��������
	if (map_lab[0][y] == 0)
		pExit.ins_coord(0, y);
	for (int y = 0; y < Y; y++)   //����� ����� ������ �� ���� ��������
	if (map_lab[X-1][y] == 0)
		pExit.ins_coord(11, y);
	for (int x = 0; x < X; x++)   //����� ����� ������ �� ���� ��������
	if (map_lab[x][Y-1] == 0)
		pExit.ins_coord(x, 31);
	for (int x = 0; x < X; x++)   //����� ����� ������ �� ���� ��������
	if (map_lab[x][0] == 0 && x != current.x)
		pExit.ins_coord(x, 0);
	while (current.x != 0 && current.y != Y-1 && current.x != X-1 && current.y != 0)
	{
		//cout << current.x << "|" << current.y; cin.get();

		delta = per_err - lab.c_back*1.0 / (lab.count + lab.c_back) * 100;
		per_err = lab.c_back*1.0 / (lab.count + lab.c_back) * 100;
		//cout << "-" << delta << "-";
		if (!lab.isLoop(current))  // ���� ������������ �����, �� ���������� ��� �������� � ������������ ��������
		{
			switch (_Pos::pr_direct(current, pExit, delta))
			{
			case 1: if (current.moveRight(&lab, map_lab))
				continue;
			case 2: if (current.moveRight(&lab, map_lab))
				continue;
				if (current.moveUp(&lab, map_lab))
					continue;
			case 3: if (current.moveRight(&lab, map_lab))
				continue;
				if (current.moveDown(&lab, map_lab))
					continue;
			case 4: if (current.moveUp(&lab, map_lab))
				continue;
			case 5: if (current.moveDown(&lab, map_lab))
				continue;
			case 6: if (current.moveLeft(&lab, map_lab))
				continue;
				if (current.moveUp(&lab, map_lab))
					continue;
			case 7: if (current.moveLeft(&lab, map_lab))
				continue;
				if (current.moveDown(&lab, map_lab))
					continue;
				if (current.moveRight(&lab, map_lab))
					continue;
				if (current.moveUp(&lab, map_lab))
					continue;
			}
		}
		else
		{
			lab.get();       //������ � ���������� ���� �� 1 ��� �����
			current = *current.next; //�������������� ������� �������
			map_lab[current.x][current.y] = 1; //��������� ��� �������
			current = *lab.get();//+ ��� ��� ������ �� 1 ��� �����
			current = *current.next; //�������������� ������� �������
			continue;
		}
		if (lab.count == 2)
		{
			cout << endl << "No way for exit!";
			break;
		}
		current = *lab.get();
		map_lab[current.x][current.y] = 1; //��������� ��� �������
		current = *current.next;            // ��������� �� ������� ������� � ������� �� �����
		//cout << current.x << "|" << current.y;cin.get();
	}
	cout << endl << lab.c_back*1.0 / (lab.count + lab.c_back) * 100 << "% of error [" << lab.count << " length][" << lab.c_back << " errors]";
	show_way(lab);
	system("pause");
	return(0);
}
bool _Pos::moveUp(Stack* way, bool arr[X][Y])
{
	if (!*(*(arr + x - 1) + y) && !(next->x == x - 1 && next->y == y))
	{
		x -= 1;
		next = way->put(x, y);
		return true;
	}
	return false;
}
bool _Pos::moveDown(Stack* way, bool arr[X][Y])
{
	if (!*(*(arr + x + 1) + y) && !(next->x == x + 1 && next->y == y))
	{
		x += 1;
		next = way->put(x, y);
		return true;
	}
	return false;
}
bool _Pos::moveRight(Stack* way, bool arr[X][Y])
{
	if (!*(*(arr + x) + y + 1) && !(next->x == x&&next->y == y + 1))
	{
		y += 1;
		next = way->put(x, y);
		return true;
	}
	return false;
}
bool _Pos::moveLeft(Stack* way, bool arr[X][Y])
{
	if (!*(*(arr + x) + y - 1) && !(next->x == x&&next->y == y - 1))
	{
		y -= 1;
		next = way->put(x, y);
		return true;
	}
	return false;
}